package com.example.practica123;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ConfiguracionActivity extends AppCompatActivity {

    Switch sw1;
    EditText ed;
    RadioGroup radg;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Configuraciones");

        sw1 = findViewById(R.id.switch1);
        ed = findViewById(R.id.editTextTamano);
        radg = findViewById(R.id.radioLetra);
        btn = findViewById(R.id.btnGuardar);

        String[] archivos = fileList();

        if (existe(archivos, "config.txt")) {
            try {
                BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("config.txt")));
                String datos = lector.readLine();
                String[] listaPersonas = datos.split(";");
                ed.setText("");
                radg.clearCheck();
                for (int i = 0; i < listaPersonas.length; i++) {
                    if (listaPersonas[i].equals("TRUE")) {
                        sw1.setChecked(true);
                    }
                    if (listaPersonas[i].equals("R.id.rbtn1")) {
                        RadioButton r = findViewById(R.id.rbtn1);
                        r.setChecked(true);
                    }
                    if (listaPersonas[i].equals("R.id.rbtn2")) {
                        RadioButton r = findViewById(R.id.rbtn2);
                        r.setChecked(true);
                    }
                    if (listaPersonas[i].equals("R.id.rbtn3")) {
                        RadioButton r = findViewById(R.id.rbtn3);
                        r.setChecked(true);
                    }
                    if (listaPersonas[i+1].equals("AVER")) {
                        ed.setText(listaPersonas[i]);
                    }
                }
                lector.close();
            } catch (Exception ex) {
                ex.printStackTrace();
                Log.e("ARCHIVO MI", "Error en la lectura del archivo " + ex.getMessage());
            }
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    deleteFile("config.txt");
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("config.txt", Activity.MODE_APPEND));
                    if (sw1.isChecked()) {
                        escritor.write("TRUE;");
                    }
                    switch (radg.getCheckedRadioButtonId()) {
                        case R.id.rbtn1:
                            escritor.write("R.id.rbtn1;");
                            break;
                        case R.id.rbtn2:
                            escritor.write("R.id.rbtn2;");
                            break;
                        case R.id.rbtn3:
                            escritor.write("R.id.rbtn3;");
                            break;
                    }
                    escritor.write(ed.getText().toString() + ";");
                    escritor.write("AVER;");
                    escritor.flush();
                    escritor.close();
                    Toast.makeText(view.getContext(), "Los datos se grabaron con éxito", Toast.LENGTH_SHORT).show();
                    finish();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Log.e("ARCHIVO MI", "Error en el archivo de escritura");
                }
            }
        });

    }

    private boolean existe(String[] archivos, String archbusca) {
        for (int f = 0; f < archivos.length; f++)
            if (archbusca.equals(archivos[f]))
                return true;
        return false;
    }
}
