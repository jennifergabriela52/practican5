package com.example.practica123;

import com.orm.SugarRecord;

public class Book extends SugarRecord<Book> {
    String title;
    String edition;



    public String getEdition() {
        return edition;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public Book(){
    }

    public Book(String title, String edition){
        this.title = title;
        this.edition = edition;
    }
}
