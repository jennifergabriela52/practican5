package com.example.practica123;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ActividadMemoriaInterna extends AppCompatActivity  implements View.OnClickListener{
    TextView cajaCedula, cajaNombres, cajaApellidos, cajaDatos;
    Button botonLeer, botonEscribir, botonConfiguraciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_memoria_interna);

        cajaCedula = (TextView) findViewById(R.id.txtCedulaMI);
        cajaApellidos= (TextView) findViewById(R.id.txtApellidosMI);
        cajaNombres = (TextView) findViewById(R.id.txtNombresMI);
        cajaDatos = (TextView) findViewById(R.id.txtDatosMI);

        botonEscribir = (Button) findViewById(R.id.btnEscribirMI);
        botonLeer = (Button) findViewById(R.id.btnLeerMI);
        botonConfiguraciones = findViewById(R.id.buttonConfiguraciones);

        botonEscribir.setOnClickListener(this);
        botonLeer.setOnClickListener(this);
        botonConfiguraciones.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnEscribirMI:
                try{
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_APPEND));
                    escritor.write(cajaCedula.getText().toString()+ ","+cajaApellidos.getText().toString()+","
                            +cajaNombres.getText().toString()+";");
                    escritor.flush();
                    escritor.close();
                }catch (Exception ex){
                    Log.e("Archivo MI", "Error en el archivo de escritura");
                }
                break;
            case R.id.btnLeerMI:
                try{
                    BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String datos = lector.readLine();
                    String [] listaPersonas = datos.split(";");
                    cajaDatos.setText("");
                    for(int i=0; i< listaPersonas.length; i++){
                        cajaDatos.append(listaPersonas[i]+"\n");
                    }

                    lector.close();
                }catch (Exception ex){
                    Log.e("Archivo MI" , "Error en la lectura del archivo" + ex.getMessage());
                }
                break;
            case R.id.buttonConfiguraciones:
                Intent intent = new Intent(ActividadMemoriaInterna.this, ConfiguracionActivity.class);
                startActivity(intent);
                break;

        }

    }
}
